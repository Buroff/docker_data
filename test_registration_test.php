<?php
/**
 *  Тестовая регистрация
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->IncludeFile(
    $APPLICATION->GetTemplatePath('include_areas/content.php'),
    array(
    ),
    array(
        'NAME'=>'Структура страницы',
        'MODE'=>'php',
    )
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");